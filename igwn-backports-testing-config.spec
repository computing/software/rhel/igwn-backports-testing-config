Name:      igwn-backports-testing-config
Version:   8
Release:   7%{?dist}
Summary:   Yum/DNF configuration for IGWN Backports Testing Repository

License:   GPLv3+
BuildArch: noarch
Requires:  redhat-release >= %{version}
Url: https://software.igwn.org/lscsoft/rocky/%{version}/backports-testing/

Source0:   igwn-backports-testing.repo
Source1:   COPYING
Source2:   README-igwn-backports-testing.md

Provides:  lscsoft-backports-testing-config = %{version}-%{release}
Obsoletes: lscsoft-backports-testing-config < 8-4

%description
Yum/DNF configuration for IGWN Backports Testing Repository on Rocky Linux %{version}

%prep
%setup -q -c -T
install -pm 644 %{SOURCE1} .
install -pm 644 %{SOURCE2} .

%build

%install
install -dm 755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README-igwn-backports-testing.md
%license COPYING
%config(noreplace) %{_sysconfdir}/yum.repos.d/igwn-backports-testing.repo

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Fri Feb 21 2025 Adam Mercer <adam.mercer@ligo.org> 8-7
- use https URLs

* Wed Feb 01 2023 Adam Mercer <adam.mercer@ligo.org> 8-6
- add source repository

* Thu Jan 05 2023 Adam Mercer <adam.mercer@ligo.org> 8-5
- fix repository description

* Thu Jan 05 2023 Adam Mercer <adam.mercer@ligo.org> 8-4
- rename to igwn-backports-testing-config

* Thu Feb 10 2022 Adam Mercer <adam.mercer@ligo.org> 8-3
- remove obsolete failovermethod

* Sat Jul 31 2021 Adam Mercer <adam.mercer@ligo.org> 8-2
- update URL for Rocky Linux

* Thu Jan 07 2021 Adam Mercer <adam.mercer@ligo.org> 8-1
- initial release
