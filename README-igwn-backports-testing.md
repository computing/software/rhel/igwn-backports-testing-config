# IGWN Backports Testing Yum/DNF Repository

The IGWN Backports Testing Yum/DNF Repository contains RPMs for testing
backported software produced by IGWN member groups.

See <https://computing.docs.ligo.org/guide/software/> for details on
IGWN Software practices.